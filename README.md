> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Developmment

## Wyatt Dell

### Project 1

> Deliverables:
1. Provide Bitbucket read-only access to p1 repo, *must* include README.md, using Markdown
syntax.

2. Blackboard Links: p1 Bitbucket repo

#### Document Files/Links: 

[LIS4368 Project 1 Webapp](http://localhost:9999/repos/lis4368/p1/index.jsp "Project 1")

#### Screetshots of Client-Side Validation:

![Failed Validation](img/fail.png)

![Passed Validation](img/pass.png)
